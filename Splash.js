import React from 'react'
import { View, Text,StyleSheet, ActivityIndicator } from 'react-native'

export default function Splash() {
    
    return (
        <View style={styles.container}>
            <Text style={styles.title}>FAST SHIP</Text>
            <Text style={styles.slogan}>Delivery is fast in anywhere</Text>
            <ActivityIndicator color='blue' animating={true} size='large'/>
            <Text>Loadding</Text>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'darkcyan'
    },
    title:{
        fontSize:30,
        fontWeight:'bold',
    },
    slogan:{
        color:'white',
        fontSize:13,
        fontStyle:'italic',
    }
});