import React from 'react';
import {View, Text, StyleSheet, Dimensions, AsyncStorage} from 'react-native';
import Header from './component/Header';
import Footer from './component/Footer';
import {ScrollView} from 'react-native-gesture-handler';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import CustomeMap from './component/CustomeMap';

const {width, height} = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default function Home({route, navigation}) {
  return (
    <ScrollView>
      <View style={StyleSheet.container}>
        {/* <Header text="WELLCOME F'SHIP"/> */}
        {/* <Footer style={styles.footer} name ={()=>(<Text>{AsyncStorage.getItem('username')}</Text>)}/> */}
        <View style={styles.mapView}>
          <CustomeMap />
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
  },
  mapView: {
    position: 'relative',
  },
  footer: {
    alignSelf: 'center',
  },
});
