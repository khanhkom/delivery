import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Title from '../component/Title'
import { TextInput, TouchableOpacity, ScrollView } from 'react-native-gesture-handler'

export default function CreateBill() {
    return (
        <ScrollView>
        <View style={styles.container}>
            <Title text="TẠO ĐƠN HÀNG" icon='cart-plus'/> 
            <View style={styles.content}>
                <View style={styles.inputContainer}>
                <TextInput style={styles.inputText,{justifyContent:'center'}} placeholder='Mã đơn:'/>
                    <Text style={styles.groupTitle}>Từ:</Text>
                    <View style={styles.inputGroup}>
                        
                        <View style={{flexDirection:'row'}}>
                            <Text style={styles.titleName}>Địa chỉ: </Text>
                            <TextInput style={styles.inputText} placeholder='Địa chỉ người gửi'/>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <Text style={styles.titleName}>Người gửi: </Text>
                            <TextInput style={styles.inputText} placeholder='Tên người gửi'/>
                        </View>
                        
                        <View style={{flexDirection:'row'}}>
                            <Text style={styles.titleName}>SĐT : </Text>
                            <TextInput style={styles.inputText} placeholder='SĐT người gửi'/>
                        </View>

                    </View>
                    <Text style={styles.groupTitle}>Đến:</Text>
                    
                    <View style={styles.inputGroup}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={styles.titleName}>Địa chỉ: </Text>
                            <TextInput style={styles.inputText} placeholder='Địa chỉ đến'/> 
                        </View>
                         <View style={{flexDirection:'row'}}>
                            <Text style={styles.titleName}>Người nhận: </Text>
                            <TextInput  style={styles.inputText} placeholder='Tên người nhận'/>
                         </View>
                         <View style={{flexDirection:'row'}}>
                            <Text style={styles.titleName}>SĐT : </Text>
                            <TextInput style={styles.inputText} placeholder='SĐT người nhận'/>
                         </View>
                    </View>
                    
                    <TextInput style={styles.inputText,{fontStyle:'italic'}} placeholder='Nội dung ghi chú'/>
                    <View style={{alignItems:'flex-end',marginRight:10}}>
                        <TouchableOpacity style={{borderRadius:20,borderColor:'deepskyblue',backgroundColor:'lightgreen'}}>
                            <Text style={{padding:12}}>Xác nhận</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
        </ScrollView> 
    )
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'lightblue',
        height:1000
    },
    inputGroup:{
        borderWidth:1,
        borderRadius:20
    },
    inputContainer:{
        margin:5,
    },
    groupTitle:{
        fontWeight:'bold',
        fontSize:25,
    },
    titleName:{
        fontSize:18,
        color:'red',
        marginTop:10,
        marginLeft:30
    },
    inputText:{
        fontSize:16,
        
    }
});