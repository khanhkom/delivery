import React,{useState} from 'react'
import { View, Text,StyleSheet, useWindowDimensions, Dimensions, Button, ScrollView} from 'react-native'
  
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/FontAwesome';
import Title from '../component/Title';
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');

export default function Feedback() {
    const [dimensions, setDimensions] = useState({ window, screen });

    return (
        <ScrollView>
        <View style={styles.container}>
            
            <View style={styles.Title}>
                <Title text='FEEDBACK' icon='reply'/>
            </View>
            
            <View style={styles.groupInput}>
                <Text style={styles.tilteInput}>Nội dung phản hồi:</Text>
                <TextInput  style={styles.inputText} placeholder="Nội dung phản hồi" multiline={true} />
                <Icon style={styles.camera} name='camera' size={20} />
            </View>
            <View style={styles.groupInput}>
                <Text style={styles.tilteInput}>Số điện thoại liên hệ:</Text>
                <View style={{flexDirection:'row'}}>
                    <TextInput value='(+84) -' editable={false} selectTextOnFocus={false} style={{fontSize:16}} /> 
                    <TextInput style={styles.inputSDT} placeholder="Số điện thoại" maxLength={13}  />
                </View> 
            </View>
            <View>
                <Button style={styles.btnGui} title='Gửi' />
            </View>
            
        </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'lightblue',
        height:screen.height,
       
    },
   
    groupInput:{
        margin:10,
        
    },
    tilteInput:{
        fontSize:20,
        fontStyle:'italic'
    },
    inputText:{
        maxHeight:150,
        height:120,
        borderWidth:1,
        borderRadius:10
    },
    camera:{
        margin:10
    },
    inputSDT:{
        fontSize:16
    },
    btnGui:{
        backgroundColor:'lightblue'
    }
});
