import React,{useState, useEffect } from 'react'
import { View, Text,StyleSheet, Modal, Alert,TouchableHighlight, AsyncStorage } from 'react-native'
import Title from '../component/Title'
import { TextInput, TouchableOpacity, ScrollView } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios'
export default function ProfileKH({navigation}) {

  var onDangxuat = ()=>{
        AsyncStorage.setItem('username');
        navigation.navigate('Login');
    }
    const [modalVisible, setModalVisible] = useState(false); 
    loadDataProfile= async ()=>{
        let res = await axios.get('https://192.168.0.110:44364/User/Getjson');
        console.log(res.data)
    }
    useEffect(()=>{
        loadDataProfile();
    });
    return (
          
        <View style={styles.container}>
            
                <Title text="CÁ NHÂN" icon='user'/>
            <View style={styles.content}>
            <ScrollView>
                <View style={styles.group}>
                    <Text style = {styles.titleInput}>Họ tên:</Text>
                    <TextInput style={styles.input} placeholder='Họ Tên' maxLength={20}/>
                </View>
                <View style={styles.group}>
                    <Text style = {styles.titleInput}>SĐT:</Text>
                    <TextInput style={styles.input} placeholder='SĐT'/>
                </View>
                <View style={styles.group}>
                    <Text style = {styles.titleInput}>Email:</Text>
                    <TextInput style={styles.input} placeholder='@mail.com'/>
                </View>
                <View style={styles.group}>
                    <Text style = {styles.titleInput}>Địa chỉ:</Text>
                    <TextInput style={styles.input} placeholder='Địa chỉ'/>
                </View>
                <View style={styles.group}>
                    <Text style = {styles.titleInput}>Ngày Sinh:</Text>
                    <TextInput style={styles.input} placeholder='Ngày'/>
                    <Text style = {styles.titleInput}>/</Text>
                    <TextInput style={styles.input} placeholder='Tháng'/>
                    <Text style = {styles.titleInput}>/</Text>
                    <TextInput style={styles.input} placeholder='Năm'/>
                </View>
                <View style={styles.group}>
                    <Text style = {styles.titleInput}>Giới tinh:</Text>
                    <TextInput style={styles.input} placeholder='Nam/Nữ'/>
                </View>
                {/* Modal */}
                <Modal animationType='slide' 
                        transparent={true}
                        visible={modalVisible}
                        > 
                    <ScrollView>
                    <View style={styles.viewModal}>
                    <View style={styles.group}>
                        <Text style = {styles.titleInput}>Họ tên:</Text>
                        <TextInput style={styles.input} placeholder='Họ Tên' maxLength={20}/>
                    </View>
                    <View style={styles.group}>
                        <Text style = {styles.titleInput}>SĐT:</Text>
                        <TextInput style={styles.input} placeholder='SĐT'/>
                    </View>
                    <View style={styles.group}>
                        <Text style = {styles.titleInput}>Email:</Text>
                        <TextInput style={styles.input} placeholder='@mail.com'/>
                    </View>
                    <View style={styles.group}>
                        <Text style = {styles.titleInput}>Địa chỉ:</Text>
                        <TextInput style={styles.input} placeholder='Địa chỉ'/>
                    </View>
                    <View style={styles.group}>
                        <Text style = {styles.titleInput}>Ngày Sinh:</Text>
                        <TextInput style={styles.input} placeholder='Ngày'/>
                        <Text style = {styles.titleInput}>/</Text>
                        <TextInput style={styles.input} placeholder='Tháng'/>
                        <Text style = {styles.titleInput}>/</Text>
                        <TextInput style={styles.input} placeholder='Năm'/>
                    </View>
                    <View style={styles.group}>
                        <Text style = {styles.titleInput}>Giới tinh:</Text>
                        <TextInput style={styles.input} placeholder='Nam/Nữ'/>
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'space-around',marginTop:20,}}>
                        <TouchableHighlight
                        underlayColor="#fff"
                            style={styles.closeBtn}
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}
                            >
                            <Text style={styles.textButton}>Thoát</Text>
                        </TouchableHighlight>
                        <TouchableHighlight
                        underlayColor="#fff"
                            style={styles.closeBtn}
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}
                            >
                            <Text style={styles.textButton}>Cập nhật</Text>
                        </TouchableHighlight>
                    </View>
                    
                    </View>
                    </ScrollView>
                </Modal>
                {/* end Modal */}

                <View style={{alignItems:'center'}}>
                <TouchableOpacity style={{backgroundColor:'dodgerblue',padding:8,borderRadius:10}}
                        onPress={
                            ()=>{setModalVisible(true)}
                        }>
                    <Text style={{color:'white'}}> <Icon name='edit' />Sửa thông tin</Text>
                </TouchableOpacity>
                </View>

                <View style={{alignItems:'center',marginTop:10}} >
                    <TouchableOpacity onPress={onDangxuat}>
                        <Text style={{textDecorationLine:'underline'}}>Đăng xuất</Text>
                    </TouchableOpacity>
                </View>
                
                </ScrollView>
            </View>
                   
           
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'lightblue',
        flex:1,
          
    },
    content:{
        
        borderWidth:0.5,
        flex:2,
        borderColor:'blue',
        borderRadius:10,
        backgroundColor:'lightblue',
    },
    group:{
        flexDirection:'row',
        padding:10
       
    },
    titleInput:{
        paddingTop:19,
        marginLeft:10,
        marginRight:10,
  
    },
    input:{
        borderBottomWidth:0.5,
        paddingBottom:1
    },
    viewModal: {
        flex: 1,
        backgroundColor:'white',
        borderRadius:20,
        justifyContent:'center',
        alignContent:'center',
        padding:15,
        marginBottom:20,
      },
    closeBtn:{
        
        alignItems:'center',
        
    },
    textButton:{
        backgroundColor:'blue',
        padding:10,
        color:'white',
        textTransform:'uppercase',
        borderRadius:20
    }
    
});
