import React from 'react'
import { View, Text,StyleSheet, Alert } from 'react-native'
import Title from '../component/Title'
import ItemBillLichsu from '../component/ItemBillLichsu'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default function HistoryBill() {
    return (
        <View style={styles.container}>
            <Title text="LỊCH SỬ ĐƠN HÀNG" icon='history'/>
            <View style={styles.content}>
                <TouchableOpacity onPress={()=>Alert.alert('Thông tin','Chi tiết đơn hàng')}>
                    <ItemBillLichsu />
                </TouchableOpacity>
                
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'lightblue',
        height:1000
    },
    content:{

    }
});