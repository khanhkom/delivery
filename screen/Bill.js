import React from 'react'
import { View, Text } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { ScrollView, FlatList } from 'react-native-gesture-handler'
import ItemConfirmOder from './component/ItemConfirmOder'
import Title from './component/Title'

const data = [
    {
        id:1,
        name:"Bùi Thu Thảo",
        address:"Quyết Tiến - Tiên Lãng - Hải Phòng",
        weight:'400g'
    },
    {
        id:2,
        name:"Lê Ngọc Ánh",
        address:"Tam Đa - Vĩnh Bảo - Hải Phòng",
        weight:'500g'
    },
    {
        id:3,
        name:"Thảo Thỏ",
        address:"Trần Tất Văn - Kiến An - Hải Phòng",
        weight:'300g'
    },
    {
        id:4,
        name:"Thảo Thỏ",
        address:"An Thắng - An Lão - Hải Phòng",
        weight:'350g'
    },
];
export default function Bill() {

    return (
        <View>
            <Title text="XEM ĐƠN HÀNG" icon='cart-arrow-down' />
        
            <ScrollView>
                <FlatList
                    data = {data}
                    renderItem = {({renderItem})=><ItemConfirmOder/>}
                />
                
            </ScrollView>
        </View>
    )
}
