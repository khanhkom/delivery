import React from 'react'
import { View, Text ,StyleSheet, Button,Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextInput, TouchableHighlight, FlatList, ScrollView } from 'react-native-gesture-handler';
import ItemConfirmOder from './component/ItemConfirmOder';
import DivTag from './component/DivTag';

const data = [
    {
        id:1,
        name:"Bùi Thu Thảo",
        address:"Quyết Tiến - Tiên Lãng - Hải Phòng",
        weight:'400g'
    },
    {
        id:2,
        name:"Lê Ngọc Ánh",
        address:"Tam Đa - Vĩnh Bảo - Hải Phòng",
        weight:'500g'
    },
    {
        id:3,
        name:"Thảo Thỏ",
        address:"Trần Tất Văn - Kiến An - Hải Phòng",
        weight:'300g'
    },
    {
        id:4,
        name:"Thảo Thỏ",
        address:"An Thắng - An Lão - Hải Phòng",
        weight:'350g'
    },
    {
        id:5,
        name:"Thảo Thỏ",
        address:"An Thắng - An Lão - Hải Phòng",
        weight:'350g'
    },

];

export default function List() {
    return (
        <SafeAreaView style={styles.containter}>
            <View style={styles.searchGroup} >
                <Text style={styles.searchTitle}>Tìm đơn hàng</Text>
                <TextInput style={styles.keyWordInput}/>
                <TouchableHighlight style={styles.btnTim}><Icon size={20} name="search" color='white'></Icon></TouchableHighlight>
            </View>
            <View style = {styles.Content}>
                <View style={styles.Mapview}>
                     {/* Mapp view Hiem thi  */}
                </View>
                {/* <DivTag nameTag="Đơn hàng nhận"/> */}
                <SafeAreaView  style={styles.ListConfirmOrder}>
                    <ScrollView>
                    <FlatList
                        keyExtractor= {item=>item.id}
                        data =  {data}
                        renderItem={({item})=><ItemConfirmOder name={item.name} id={item.id} address={item.address} weight={item.weight}/>}
                    >
                    </FlatList>
                    
                    {/* Danh sách đơn hàng Hiem thi  */}
                    </ScrollView>
                </SafeAreaView >
                <DivTag nameTag="Map"/>
                  
            </View>
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    containter:{
        flex:1,
        // backgroundColor:'royalblue'
    },
    searchGroup:{
        flexDirection:'row',
        marginTop:5,
        marginLeft:5,
        marginRight:4,
        padding:8,
        backgroundColor:'#6495ed',
        borderWidth:0.25,
        borderColor:'white',
        borderTopRightRadius:10,
        borderTopLeftRadius:10,

    },
    searchTitle:{
        marginTop:5,
        marginRight:5,
        color:'white'
    },
    keyWordInput:{
       borderWidth:1,
       height:35,
       width:250,
       borderRadius:15,
       borderColor:'blue',
       backgroundColor:'white',
       fontStyle:'italic',
       fontWeight:'normal'
    },
    btnTim:{
        marginTop:9,
        marginStart:10
    }

});
