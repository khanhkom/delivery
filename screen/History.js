import React from 'react'
import { View, Text,StyleSheet } from 'react-native'
import ItemConfirmOder from './component/ItemConfirmOder'
import ItemBillLichsu from './component/ItemBillLichsu'
import Title from './component/Title'
import { ScrollView } from 'react-native-gesture-handler'


export default function History() {
    return (
        <View style={styles.container}>
            <Title text= "LỊCH SỬ" icon='history' />
            <ScrollView>
                <ItemBillLichsu />    
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'lightskyblue',
    },

});
