import React, {useState, useEffect} from 'react';
import {View, Text, Image, TextInput, StyleSheet, Alert} from 'react-native';
import {
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function Login({navigation, title}) {
  const [email, setuseremail] = useState('');
  const [password, setpassword] = useState('');
  const [KH_Selected, setKH_Selected] = useState(false);
  const [Shipper_Selected, setShipper_Selected] = useState(false);

  const onLogin = () => {
    navigation.replace('NavKH');
  };
  const onRegister = () => {
    navigation.replace('Register');
  };
  useEffect(() => {});
  return (
    <ScrollView>
      <View style={styles.container}>
        <Image style={styles.tiny} source={require('../assets/logo.png')} />

        <View style={styles.inputContainer}>
          <Icon name="user-circle" size={20} style={styles.inputIcon} />
          <TextInput
            style={styles.input}
            placeholder="Email"
            keyboardType="email-address"
            onChangeText={email => setuseremail(email)}
          />
        </View>

        <View style={styles.inputContainer}>
          <Icon name="eye-slash" size={20} style={styles.inputIcon} />
          <TextInput
            style={styles.input}
            placeholder="Password"
            textContentType="password"
            maxLength={8}
            secureTextEntry={true} /// mã hóa pass
            onChangeText={email => setpassword(password)}
          />
        </View>
        <TouchableOpacity
          style={[styles.buttonContainer, styles.loginButton]}
          onPress={onLogin}>
          <Text style={styles.loginText}>Login</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => {
            Alert.alert('Title', 'Forgot password.');
          }}>
          <Text style={styles.textBtn}>Quên mật khẩu</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonContainer} onPress={onRegister}>
          <Text style={styles.textBtn}>Đăng kí </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 780,
    backgroundColor: 'lightseagreen',
  },
  tiny: {
    width: 300,
    height: 200,
    marginBottom: 20,
  },
  inputContainer: {
    //   borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 0.5,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  inputIcon: {
    marginTop: 12,
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center',
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
  },
  loginButton: {
    backgroundColor: '#00b5ec',
  },
  loginText: {
    color: 'white',
  },
  textBtn: {
    textDecorationLine: 'underline',
  },
});
