import React,{useState} from 'react'
import { View, Text,StyleSheet,TextInput ,Image, TouchableOpacity,Picker, Alert,Button, CheckBox} from 'react-native'
import Title from './component/Title'
import { ScrollView } from 'react-native-gesture-handler';
import DateTimePicker from '@react-native-community/datetimepicker';
import Axios from 'axios';

import { set, Value } from 'react-native-reanimated';
export default function register({navigation}) {
    //
const [date, setdate] = useState(new Date().getDate());
const [mode, setMode] = useState('date');
//infor register
const [name, setname] = useState('');
const [birthday, setbirthday] = useState('');
const [username, setusername] = useState('');
const [password, setpassword] = useState('');
const [gioitinh, setgioitinh] = useState(1);
const [diachi, setdiachi] = useState('');
const [sdt, setsdt] = useState('');
const [email, setemail] = useState('');
const [funtion, setfuntion] = useState(0);
//const [status, setstatus] = useState('');

//
const url = 'http://192.168.43.190:8888'

const onChange = (event, selectedDate) => {
  const currentDate = selectedDate || date;
  setshow(true);
  setDate(currentDate);
  
};

const showMode = currentMode => {
    setshow(true);
  setMode(currentMode);
  
};

// const [password, setpassword] = useState('');
const [rePassword, setrePassword] = useState('')
//////////////
const [dayValue, setdayValue] = useState(1);
const [monthValue, setmonthValue] = useState(1);
const [yearValue, setyearValue] = useState(1956)
//
const [modalVisible, setModalVisible] = useState(false);

///show Dattimepicker
const [show, setshow] = useState(false);

const checkVertifyPassword = (password,rePassword)=>{
    if((password!==null)&&(rePassword!==null)){
        if(password!==rePassword){
            //alert('Mật khẩu không khớp.');
            return false;
        }
        else{
            return true;
        }
    }
    return false;
}

//
const onChangDate = (event,selectedDate)=>{
 const currentDate = selectedDate||date;
 setdate(currentDate);
}

const onDangki=()=>{
    // if(checkVertifyPassword){
    //    alert('Mật khẩu không trùng khớp.');
    // }else{

    // }
    Axios.post(url + '/User/RegisterRequets?name='
                 +name+
                '&birthday='+birthday+
                '&username='+username+
                '&password='+password+                                             
                '&gioitinh='+gioitinh+
                '&diachi='+diachi+
                '&sdt='+sdt+
                '&email='+email+
                '&funtion='+funtion
                //status: null
    )
    .then((res) => {
        alert(JSON.stringify(res.data));
        // const userData = {
        //     user_id: null,
        //     name: null,
        //     brith_day: null,
        //     user_name: null,                                       
        //     password: null,                                              
        //     gioitinh: false,
        //     diachi: null,
        //     sdt: null,
        //     email: null,
        //     funtion: null,
        //     status: null
        // }
        // var dataString = JSON.stringify(res);
        // var dataObj = JSON.parse(dataString);//** */
        // for (let i = 0; i < dataObj.data.length; i++) {
        //    alert(dataObj.data[i].user_id) 
        // }
       
        
    })
    .catch((err) => {
        //console.log('Da xay ra loi', err);
        alert(err);
    })
}



    return (
        <ScrollView>  
        <View style={{justifyContent:'center'}}>
                <Title text="ĐĂNG KÍ" icon='user-plus'/>
            <View style={{padding:10}}>
                <View style={styles.groupInput}>
                    <Text style={styles.titleInput}>Họ và tên:</Text>
                    <TextInput style={styles.nameInput}
                        onChangeText={(name)=>{setname(name)}}
                        placeholder="Họ và tên"/>
                </View>
                <View style={styles.groupInput}>
                    <Text style={styles.titleInput}>SĐT:</Text>
                    <TextInput  style={styles.nameInput}
                    onChangeText={(sdt)=>{setsdt(sdt)}}
                        placeholder="Số điện thoại"/>
                </View>
                <View style={styles.groupInput}>
                    <Text style={styles.titleInput}>Email:</Text>
                    <TextInput style={styles.nameInput}
                    onChangeText={(email)=>{setemail(email)}}
                        placeholder="@.com" />
                </View>
                <View style={styles.groupInput}>
                    <Text style={styles.titleInput}>Ngày sinh:</Text>
                    <TextInput style={styles.nameInput}
                    onChangeText={(ngaysinh)=>{setbirthday(ngaysinh)}}
                        placeholder="day/month/year"/>
                </View>
                <View style={styles.groupInput}>
                    <Text style={styles.titleInput}>Username:</Text>
                    <TextInput style={styles.nameInput}
                    onChangeText={(username)=>{setusername(username)}}
                        placeholder="Username" />
                </View>
                
                        <View style={styles.groupInput}>
                            <Text style={styles.titleInput}>Mật khẩu:</Text>
                            <TextInput style={styles.nameInput}
                                         placeholder="Nhập mật khẩu"
                                         onChangeText={(password)=>{setpassword(password)}}
                                         secureTextEntry={true}/> 
                        </View>
                        <View style={styles.groupInput}>
                            <Text style={styles.titleInput}>Nhập lại mật khẩu:</Text>
                            <TextInput style={styles.nameInput}
                            placeholder='Nhập lại mật khẩu'
                            onChangeText={(rePassword)=>setrePassword(rePassword)}
                            secureTextEntry={true}/>
                        </View>
               
                <View style={styles.groupInput}>
                    <Text style={styles.titleInput}>Địa chỉ:</Text>
                    <TextInput style={styles.nameInput}
                        onChangeText={(diachi)=>{setdiachi(diachi)}}
                        placeholder="Địa chỉ" />
                </View>
                   
                    
                    <Picker 
                    style={{width:146}}
                        selectedValue={gioitinh}
                        onValueChange={(value,index)=>setgioitinh(value)}
                    >
                        <Picker.Item label='Nam' value={1}/>
                        <Picker.Item label='Nữ' value={0}/>
                    </Picker>
                    <Picker
                    style={{width:146}}
                        selectedValue={funtion}
                        onValueChange={(value,index)=>setfuntion(value)}
                    >
                        <Picker.Item label='Shipper' value={0}/>
                        <Picker.Item label='Customer' value={1}/>
                    </Picker>
            
                <View style={{flexDirection:'row',justifyContent:'space-around'}}>
                <TouchableOpacity style={styles.btnDangki}
                    onPress={onDangki}>
                        <Text style={{textAlign:'center',color:'white',borderRadius:10,padding:15,marginTop:5,backgroundColor:'cornflowerblue'}}>Đăng kí</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnDangki}
                    onPress={()=>{navigation.goBack()}}>
                        <Text style={{textAlign:'center',color:'black',marginTop:20,textDecorationLine:'underline',fontSize:15}}>Trở về</Text>
                </TouchableOpacity>  
                </View>
              
            </View>    
                        
        </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    group:{
        borderWidth:2,
      
        borderTopEndRadius:20,
        borderBottomLeftRadius:20,
        borderColor:'white',
        marginLeft:8,
        padding:20,
        backgroundColor:'slategray'
    },
    titleInput:{
        marginLeft:10,
        marginTop:12,
        
        fontFamily:'Sans-serif',
        fontSize:20,
        marginRight:10,
    },
    groupInput:{
        flexDirection:'row',
        borderRadius:10,
        borderColor:'black',
        borderBottomWidth:1,
        
        marginTop:10
    },
    nameInput:{
        marginTop:3,
        fontSize:20
    },
    day:{
        marginTop:2
    },
     line:{
        marginTop:14,
        fontSize:14,
        marginLeft:20,
        marginRight:20
    },
    passwordContainer:{

    },
    btnDangki:{
        justifyContent:'center',
        alignItems:'center',
        
    }
    
});