import React,{useState}from 'react'
import { View, Text,StyleSheet, Dimensions, CheckBox ,Button, KeyboardAvoidingView, TouchableWithoutFeedback,Keyboard, TouchableOpacity,TouchableHighlight, Alert, Modal, AsyncStorage} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import DateTimePicker from '@react-native-community/datetimepicker';
import Title from './component/Title';
import Icon from 'react-native-vector-icons/FontAwesome';
const {width,height}= Dimensions.get('window');
const ASPECT_RATIO = width / height;
export default function Account({navigation}) {
    const [checkNam, setcheckNam] = useState(false);
    const [checkNu, setcheckNu] = useState(false);

    
  
//
const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false);
    setDate(currentDate);
    
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
    
  };

  const showDatepicker = () => {
    showMode('date');
  };
//////////////
const [dayValue, setdayValue] = useState(1);
const [monthValue, setmonthValue] = useState(1);
const [yearValue, setyearValue] = useState(1956)
//
const [modalVisible, setModalVisible] = useState(false);
const onChangeDay = ()=>{
    if(dayValue >31){
        setdayValue(31);
    }
}

const logOut = ()=>{
   
    navigation.replace('Login');
    AsyncStorage.setItem('username',null);
}
    return (
        <ScrollView style={styles.container}>
    <View >
        
            
            <Title text = 'THÔNG TIN CÁ NHÂN' icon='user' size={20} />
            <View style={styles.group}>
            <TouchableOpacity
                style={styles.openModal}
                onPress={() => {
                setModalVisible(true);
                }}
                >
                    <View style={styles.groupEdit}>
                        <Icon name='pencil' size={20} style={{marginLeft:5}} />
                        <Text style={styles.modalText }>Sửa thông tin</Text>
                    </View>
               
            </TouchableOpacity>

            <Modal animationType='slide'
               transparent={true}
               visible={modalVisible}
             
               >
                    <View style={styles.centeredView}>
                                <View style={styles.modalView}> 
                        <View>
                                    <View style={styles.groupInput}>
                                <Text style={styles.titleInput}>Họ và tên:</Text>
                                <TextInput style={styles.nameInput}
                                    placeholder="Họ và tên"/>
                            </View>
                            <View style={styles.groupInput}>
                                <Text style={styles.titleInput}>SĐT:</Text>
                                <TextInput  style={styles.nameInput}
                                    placeholder="Số điện thoại"/>
                            </View>
                            <View style={styles.groupInput}>
                                <Text style={styles.titleInput}>Email:</Text>
                                <TextInput style={styles.nameInput}
                                    placeholder="@.com" />
                            </View>
                            <View style={styles.groupInput}>
                                <Text style={styles.titleInput}>Địa chỉ:</Text>
                                <TextInput style={styles.nameInput}
                                    placeholder="Địa chỉ" />
                            </View>
                            <View style={styles.groupInput}>
                    <Text style={styles.titleInput}>Ngày sinh:</Text>
                    
                    <TextInput style={styles.day} 
                        placeholder='Ngày'
                        keyboardType='number-pad'
                       
                        onChangeText={onChangeDay}
                        maxLength={2}
                    />
                    <Text style={styles.line}>-</Text>
                    <TextInput style={styles.day} 
                        placeholder='Tháng'
                        keyboardType='number-pad'
                       
                        maxLength={2}
                    />
                    <Text style={styles.line}>-</Text>
                    <TextInput style={styles.day} 
                        placeholder='Năm'
                        TextInputDisableStatus={true}
                        keyboardType='number-pad'
                      
                        maxLength={4}
                    />
                </View>

                </View>
                    <View style={{flexDirection:'row',justifyContent:'space-around'}}>
                    <TouchableHighlight
                    style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                    onPress={() => {
                        setModalVisible(!modalVisible);
                    }}
                    >
                        <Text style={styles.textStyle}>Hủy bỏ</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                    style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                    onPress={() => {
                        setModalVisible(!modalVisible);
                    }}
                    >
                        <Text style={styles.textStyle}>Xác nhận</Text>
                    </TouchableHighlight>
                    </View>
                </View>
            </View>


               </Modal>
               
                <View style={styles.groupInput}>
                    <Text style={styles.titleInput}>Họ và tên:</Text>
                    <TextInput style={styles.nameInput}
                        placeholder="Họ và tên"/>
                </View>
                <View style={styles.groupInput}>
                    <Text style={styles.titleInput}>SĐT:</Text>
                    <TextInput  style={styles.nameInput}
                        placeholder="Số điện thoại"/>
                </View>
                <View style={styles.groupInput}>
                    <Text style={styles.titleInput}>Email:</Text>
                    <TextInput style={styles.nameInput}
                        placeholder="@.com" />
                </View>
                <View style={styles.groupInput}>
                    <Text style={styles.titleInput}>Địa chỉ:</Text>
                    <TextInput style={styles.nameInput}
                        placeholder="Địa chỉ" />
                </View>
                <View style={styles.groupInput}>
                    <Text style={styles.titleInput}>Ngày sinh:</Text>
                    
                    <TextInput style={styles.day} 
                        placeholder='Ngày'
                        keyboardType='number-pad'
                       
                        onChangeText={onChangeDay}
                        maxLength={2}
                    />
                    <Text style={styles.line}>-</Text>
                    <TextInput style={styles.day} 
                        placeholder='Tháng'
                        keyboardType='number-pad'
                       
                        maxLength={2}
                    />
                    <Text style={styles.line}>-</Text>
                    <TextInput style={styles.day} 
                        placeholder='Năm'
                        TextInputDisableStatus={true}
                        keyboardType='number-pad'
                      
                        maxLength={4}
                    />
                </View>
            </View>

            <View style={styles.groupbutton}>
                <TouchableOpacity style={styles.dangxuatbtn}
                onPress={logOut} >
                    <Text>Đăng xuất</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.doimatkhaubtn} 
                >
                    <Text>Đổi mật khẩu</Text>
                </TouchableOpacity>
            </View>
        </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'lightskyblue'
    },
    group:{
        borderWidth:2,
        
        marginHorizontal:20,
        borderTopEndRadius:20,
        borderBottomLeftRadius:20,
        borderColor:'white', 
    },
    groupInput:{
        flexDirection:'row'
    },
    titleInput:{
        marginLeft:10,
        marginTop:12,
        
        fontFamily:'Sans-serif',
        fontSize:15,
        marginRight:10,
    },
    nameInput:{

    },
    groupCheckbox:{
        flexDirection:'row'
    },
    titleCheck:{
        marginTop:4
    },
    line:{
        marginTop:14,
        fontSize:14,
        marginLeft:20,
        marginRight:20
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },    
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 4
      },
      openModal:{

        padding: 10,

        
      },
      modalText: {
        marginBottom: 15,

        
      },
      groupbutton:{
          flexDirection:'row',
          justifyContent:'space-around',
          marginVertical:70,
      },
      dangxuatbtn:{
          borderRadius:20,
          padding:10,
          backgroundColor:'lightgreen',
      },
      doimatkhaubtn:{
        borderRadius:20,
        padding:10,
        backgroundColor:'lightgreen',
      },
      groupEdit:{
          flexDirection:'row',
      }
});
