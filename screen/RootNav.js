import React from 'react'
import { View, Text } from 'react-native'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Login from './Login';
import Home from './Home';
import List from './List';
import Bill from './Bill';
import DetailBill from './DetailBill';
import Accountt from './Account';
import History from './History';
import manageBill from './shipper/manageBill';
import register from './register';
import HomeKH from './customer/HomeKH';
import HistoryBill from './customer/HistoryBill';
import CreateBill from './customer/CreateBill';
import tabBar from './component/tabBar';
import Feedback from './customer/Feedback';
import ProfileKH from './customer/ProfileKH';
import navLogin from './navLogin';
import KH_login from'../screen/KH_login'
import LoginScreen from './LoginScreen'
const stack = createStackNavigator();
const tabs = createBottomTabNavigator();
const tabsTop = createMaterialTopTabNavigator();
export default function RootNav() {

    return (
    <NavigationContainer>
        <stack.Navigator initialRouteName='Login' headerMode="none" >
            <stack.Screen name='Home' options={{
              activeTintColor:'red',
              headerTitleAlign:'center',
              icactiveTintnColor:'black',
              headerStyle:{
                backgroundColor:'#00d'
              }
            }}>
              {////
                ()=>(
                  <tabs.Navigator initialRouteName='Home' >
                      {/* <tabs.Screen name="Trang chủ" component={Home} options={{tabBarIcon:({color})=><Icon name='home' size={30} color={color}/>}}/> */}
                      <tabs.Screen name="Quản lí" options={{tabBarIcon:({color})=><Icon name='database' size={30} color={color}/>}}>
                          {
                            ()=>(
                               <tabsTop.Navigator initialRouteName="Xem đơn hàng">
                                <tabsTop.Screen name='Xem đơn hàng' component={Bill} options={{tabBarIcon:({color})=><Icon name ='bill' size={30} color={color}/>}}></tabsTop.Screen>
                                <tabsTop.Screen name='Lịch sử đơn' component={History}></tabsTop.Screen>
                               </tabsTop.Navigator> 
                            )
                          }
                      </tabs.Screen>
                      <tabs.Screen name="Profile" component={Accountt} options={{tabBarIcon:({color})=><Icon name='user' size={30} color={color}/>}}/>
                  </tabs.Navigator>
                )//////
              }
            </stack.Screen>

            <stack.Screen name='Login' component={LoginScreen}>
            </stack.Screen>
            <stack.Screen name='Detail' component={DetailBill}/>
            <stack.Screen name='Register' component={register}/>
            <stack.Screen name='KHHome'>
              {
                ()=>(
                  <tabs.Navigator initialLayout='Cá nhân'
                      >
                    {/*  */}
                    <tabs.Screen name='Quản lí đơn hàng' options={{tabBarIcon:({color})=><Icon name='tasks' size={30} color={color}/>}}>
                      {
                        ()=>(
                         <tabsTop.Navigator {...tabsTop} initialRouteName='Trang chủ' 
                         options={{title:'Shipper lân cận'}} 
                         tabBarOptions={{inactiveTintColor:'deepskyblue',
                         pressColor:'gray',
                         headerStyle:{
                          backgroundColor: '#f4511e',
                         }
                         }}
                          >
                           <tabsTop.Screen name="Trang chủ" component={Home} options={{tabBarIcon:({color})=><Icon name='home' size={30} color={color}/>}}/>
                            <tabsTop.Screen name='Tạo đơn hàng' component={CreateBill}/>
                            <tabsTop.Screen name='Lịch sử đơn hàng' component={HistoryBill}/>
                         </tabsTop.Navigator>
                        )
                      }
                    </tabs.Screen>
                    <tabs.Screen name='Cá nhân' component={ProfileKH} options={{tabBarIcon:({color})=><Icon name='user-circle' size={30} color={color}/>}}/>
                    <tabs.Screen name='Phản hồi' component={Feedback} options={{tabBarIcon:({color})=><Icon name='reply-all' size={30} color={color}/>}}/>
                  </tabs.Navigator>
                )
              }
            </stack.Screen>
            
        </stack.Navigator>
  </NavigationContainer>
    );
}
