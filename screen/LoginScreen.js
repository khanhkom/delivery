import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  StatusBar,
  KeyboardAvoidingView,
  AsyncStorage,
} from 'react-native';
import {
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native-gesture-handler';
import axios from 'axios';
let baseUrl = 'http://192.168.43.190:8888';
export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
    };
  }

  Login = async () => {
    //this.props.navigation.navigate('Home',{name:this.state.email});
    //alert(this.state.username+this.state.password+'jj');
    this.props.navigation.replace('KHHome');
    axios
      .post(
        baseUrl +
          '/User/Loginrequest?username=' +
          this.state.username +
          '&password=' +
          this.state.password,
        //    ,{
        //     username:this.state.username,
        //     password:this.state.password,
        //    }
      )
      .then(res => {
        res => JSON.stringify(JSON.parse(res));
        // alert(JSON.stringify(res.data));
        if (res.data.length > 0) {
          if (res.data[0].funtion == 1) {
            AsyncStorage.setItem('username', res.data[0].username);
            this.props.navigation.replace('Home');
          } else if (res.data[0].funtion == 0) {
            AsyncStorage.setItem('username', res.data[0].username);
            this.props.navigation.replace('KHHome');
          } else {
            alert('Tài khoản không chính xác.');
          }
        } else {
          alert('Kiem tra lai tai thong tin');
        }
      });
  };

  componentDidMount() {
    // this._loadInitialState();
  }
  _loadInitialState = () => {
    var value = AsyncStorage.getItem('username');
    if (value !== null) {
      this.props.navigation.navigate('Home', {value: value});
    }
  };
  render() {
    return (
      <ScrollView style={styles.container}>
        <View>
          <StatusBar hidden={true} />
          <View style={styles.circle} />
          <View style={{marginTop: 64}}>
            <Image
              source={require('../assets/logo.png')}
              style={{width: 200, height: 200, alignSelf: 'center'}}
            />
          </View>
          <View style={{marginHorizontal: 32}}>
            {/* <Text style={styles.header} >LOGIN</Text> */}
            <TextInput
              style={styles.input}
              placeholder="username"
              onChangeText={username => {
                this.setState({username});
              }}
              value={this.state.username}
            />
            <TextInput
              style={styles.input}
              placeholder="Password"
              onChangeText={password => {
                this.setState({password});
              }}
              value={this.state.password}
              secureTextEntry
            />
            <View style={{alignItems: 'center', marginTop: 64}}>
              <TouchableOpacity style={styles.continue} onPress={this.Login}>
                <Text style={{color: 'white'}}>Login</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 60,
              }}>
              <TouchableOpacity
                style={styles.buttonContainer}
                onPress={() => {
                  alert('Forgot password.');
                }}>
                <Text style={styles.textBtn}>Quên mật khẩu ?</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonContainer}
                onPress={() => this.props.navigation.navigate('Register')}>
                <Text style={styles.textBtn}>Đăng kí </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightskyblue',
  },
  circle: {
    width: 500,
    height: 500,
    borderRadius: 500 / 2,
    backgroundColor: '#FFF',
    position: 'absolute',
    left: -120,
    top: -10,
  },
  header: {
    fontWeight: '800',
    fontSize: 30,
    color: '#514E5A',
    marginTop: 32,
    alignItems: 'center',
  },
  input: {
    marginTop: 10,
    height: 50,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#BAB7C3',
    borderRadius: 30,
    paddingHorizontal: 16,
    color: 'black',
    fontWeight: '600',
    borderWidth: 2,
  },
  continue: {
    width: 70,
    height: 70,
    borderRadius: 70 / 2,
    fontWeight: 'bold',
    fontSize: 30,
    backgroundColor: 'dodgerblue',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
