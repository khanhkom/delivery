import React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

export default function navLogin() {
  const topTab = createMaterialTopTabNavigator();
  return (
    <NavigationContainer>
      <topTab.Navigator>
        <topTab.Screen name="Khách hàng" />
        <topTab.Screen name="Shipper" />
      </topTab.Navigator>
    </NavigationContainer>
  );
}
