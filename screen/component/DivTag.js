import React from 'react'
import { View, Text,StyleSheet } from 'react-native'

export default function DivTag({nameTag}) {
    return (
        <View style = {styles.container}>
            <Text>{nameTag}</Text>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'darkgray',
        alignContent:'center',
        marginLeft:5,
        alignItems:'center',
        borderRadius:20,
        marginTop:15
    }
});