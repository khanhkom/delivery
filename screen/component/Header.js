import React from 'react'
import { View, Text,StyleSheet,Image } from 'react-native'

export default function Header({text}) {
    return (
        <View style={styles.container}>
             <Image style={styles.tiny}
                    source={{uri:'https://i.pinimg.com/originals/8a/a4/59/8aa4595fb24b6ed585dddac4622b2445.gif'}} />
            <Text style={styles.title}>{text}</Text>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        fontSize:30,
        margin:2,
        // borderWidth:1,
        // borderRadius:10,
        borderColor:'black',
    
    },
    tiny:{
        width:50,
        height:50,
        margin:10,
    },
    title:{
        fontSize:30,
        margin:10,
    }
});
