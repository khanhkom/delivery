import React from 'react'
import { View, StyleSheet ,Image} from 'react-native'
import { Marker } from 'react-native-maps'
import Icon from 'react-native-vector-icons/FontAwesome';
export default function CustomeMarker({coordinate,title,description}) {
    return (
        <View>
            <Marker title={title} description={description} coordinate={coordinate}/>
            <Icon name='archive' size={35} color='blue' />
            {/* <Image style={styles.tiny}
            source={require('../../assets/package.jpg')} /> */}
        </View>
    )
}

const styles = StyleSheet.create({
    tiny:{
        width:30,
        height:35,
    },
});