import React, { useEffect } from 'react'
import { View, Text ,StyleSheet, Alert,TouchableOpacity} from 'react-native'
import { TouchableHighlight } from 'react-native-gesture-handler'
import Title from './Title'
import { color } from 'react-native-reanimated'
import Icon from 'react-native-vector-icons/FontAwesome';



export default function ItemConfirmOder({time,id,shipper_id,custome_id,name_receive,address_receive,address_send,phone_receive,status},{navigation}) {
    
    const onDetail=()=>{
         Alert.alert('title','Chi tiết đơn hàng');
        // navigation.navigate('Detail');
    }
  
    return (
        <View style={styles.container}>
                
            {/* Don hang item */}
            <View style={styles.groupContent}>
                <View style ={styles.billTitle}>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{fontSize:25}}>Mã đơn:{id} </Text>
                        <TouchableOpacity style={styles.groupButton}>
                            {/* <Text style={styles.detail}>Detail</Text> */}
                            <Icon name='outdent' size={40} style={{marginLeft:240,marginTop:20}} color='mediumblue' />
                        </TouchableOpacity>
                    </View>
                    <View>
                        <Text style={{fontSize:15,textDecorationLine:'underline'}} >Gửi từ: {address_send}</Text> 
                        <Text  style={{fontSize:15,textDecorationLine:'underline'}}>Nhận: {address_receive}</Text> 
                        {/* <View>
                            <Text>Km</Text>
                        </View> */}
                    </View>
  
                    
{/* 
                    <Text>Người gửi:</Text> 
                    <Text>Người nhận:</Text>

                <Text>Sdt gửi :</Text>
                <Text>Sdt nhận:</Text> */}
                
                

                
                {/* <Text>Giá đơn hàng:</Text>
                <Text>Phí vận chuyển:</Text>

                <Text>Thành tiền/Tổng: </Text> */}

                <View  style={styles.groupButton}>
                    {/* <TouchableOpacity>
                        <Text style={styles.accept}>Accept</Text>
                        
                    </TouchableOpacity> */}
                    {/* <TouchableOpacity>
                        <Text style={styles.cancel}>Cancel</Text>
                    </TouchableOpacity> */}
                    
                </View>
                </View>

            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'gray',

    },
    groupContent:{

        borderTopWidth:1,
        borderBottomWidth:1,
        borderRadius:10

    },
    billTitle:{
        ///margin:5,

    },
    groupButton:{
        flexDirection:'row',
        alignContent:'space-between'
        
    },
   
    detail:{
       
        padding:3,
        color:'blue',
        marginLeft:220,
        fontSize:20,
        textAlign:'center',
        marginRight:10,
    }
});
