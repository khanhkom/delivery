import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Title from './Title'

export default function tabBar({text}) {
    return (
        <View style={styles.container}>
            <Title text = {text} ></Title>
        </View>
    )
}
const styles = StyleSheet.create({

});