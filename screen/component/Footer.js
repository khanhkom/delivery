import React,{useState} from 'react'
import { View, Text,StyleSheet,Image, Dimensions,Switch} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome';

export default function Footer({name,numberStar}) {

// const origin = {latitude: 37.3318456, longitude: -122.0296002};
// const destination = {latitude: 37.771707, longitude: -122.4053769};
// const GOOGLE_MAPS_APIKEY = '…';
const [isEnabled, setIsEnabled] = useState(false);
const toggleSwitch = () => setIsEnabled(previousState => !previousState)
    return (
        
        <View style={styles.container}>
            <View style = {styles.content}>
                <View style={styles.grAvt}>
                    <Image style={styles.img} source = {{uri:'https://i.ya-webdesign.com/images/businessman-png-icon-1.png'}} />
                    
                    <View style={styles.group1}>
                        <Text style={styles.name}>{name}</Text>
                        <Text style={styles.tien}>{0} VNĐ</Text>
                    </View>
                    <View style={styles.group2}>
                        <Text style={styles.rank}>Xếp hạng</Text>
                        <Text style={styles.star}>{5} <Icon name='star' size={20} color='orange' /></Text>
                    </View>
                    <View >
                        <Text style={styles.activeTitle}>Nhận đơn</Text>
                        <Switch 
                        trackColor={{ false: "darkgrey ", true: "white" }}
                        thumbColor={isEnabled ? "white" : "lime"}
                        ios_backgroundColor="#9fa"
                        onValueChange={toggleSwitch}
                        value={isEnabled}
                        />
                    </View>
                    
                    
                </View>
                <Text style={styles.sdt}>SĐT:</Text>
            </View>
            
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        flexDirection:'column',
        marginLeft:1,
        marginRight:1,
        borderColor:'white',
        borderTopRightRadius:15,
       
    },
    content:{
       flexDirection:'column',
       borderWidth:1,
       borderColor:'white',
       borderTopRightRadius:20,
       borderTopLeftRadius:20,
       backgroundColor:'lightseagreen',
       
       
    },
    grAvt:{
        flexDirection:'row',
        alignContent:'space-around',
        justifyContent:'space-around'
    },
    img:{
        width:50,
        height:50,
        marginTop:18,
        marginLeft:22,
    },
    name:{
        alignContent:'flex-start',
        fontStyle:'normal',
        textAlign:'left',
        fontSize:20,
        marginLeft:25,
        marginTop:16

    },
    group1:{
        marginTop:1,
    },
    rank:{
        fontStyle:'normal',
        alignSelf:'flex-end',
        fontSize:15,
        marginTop:21,
        
    },
    star:{
        alignItems:'flex-end',
        marginLeft:60
    },
    group2:{
        alignSelf:'flex-end'
    },
    sdt:{
        marginTop:5,
        marginLeft:30
    },
    tien:{
        marginLeft:27
    },
    activeTitle:{
        marginTop:24,
        marginLeft:40,
    }
    
});