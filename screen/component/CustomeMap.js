import React, { Component } from 'react'
import { Text, View,StyleSheet,Dimensions } from 'react-native'
import MapView, { Marker } from 'react-native-maps'
import Icon from 'react-native-vector-icons/FontAwesome';
import CustomeMarker from './CustomeMarker';

export default class CustomeMap extends Component {
    constructor(props){
     super(props);
     this.state={
         region:{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
         },
         marker:{
            latitude: 20.8250,
            longitude: 106.63790,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
            title:'Đơn hàng',
            description:'Mô tả'
         },
         currentLoc:{
            latitude:  20.8100,
            longitude: 106.6710,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
            title:'Đơn hàng',
            description:'Mô tả'
         }
     }
    }
    componentDidMount(){

    }
    render() {
        return (
                <MapView
                style={styles.container}
                    initialRegion={this.state.marker}
                      showsUserLocation={true}
                      showsMyLocationButton={true}
                      showsScale={true}
                >
                    <Marker coordinate={this.state.marker}
                        title={this.state.marker.title}
                        description={this.state.marker.description}
                    >
                        <CustomeMarker {...this.state.marker}/>
                    </Marker>

                    <Marker coordinate={this.state.currentLoc}
                        title={this.state.currentLoc.title}
                        description={this.state.currentLoc.description}
                    >
                        <CustomeMarker {...this.state.currentLoc}/>
                    </Marker>

                    <Marker coordinate={{
                        latitude:  20.8200,
                        longitude: 106.6740,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                        title={this.state.currentLoc.title}
                        description={this.state.currentLoc.description}
                    >
                        <CustomeMarker {...this.state.currentLoc}/>
                    </Marker>
                   <View style={{backgroundColor:'red',position:'absolute'}}></View>
                </MapView>
        )
    }
}
const {width,height}= Dimensions.get('window');
const {d,r} = Dimensions.get('screen');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const styles = StyleSheet.create({
    container:{
        flex:1,
        width:width,
        height:height,
    },
    
});