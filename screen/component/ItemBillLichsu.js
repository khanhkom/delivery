import React from 'react'
import { View, Text,StyleSheet } from 'react-native'

export default function ItemBillLichsu({madon,SDTnhan,Thoigian,nguoigiao,Trangthai}) {
    return (
        <View style={styles.container}>
            <View style={{margin:8}}> 
                <Text style={styles.titleText}>Mã đơn:{madon}</Text>
                <Text style={styles.sdtNhan}> SĐT Người nhận:{SDTnhan}</Text>

                <Text style={styles.sdtNhan}> Người giao:{nguoigiao}</Text>
            </View>
            <View style={{flexDirection:'row',justifyContent:'space-around'}}>
                <Text>{Thoigian}</Text>
                <Text>{Trangthai}</Text>
            </View>
        </View>
    )
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:'lavenderblush',
        borderTopWidth:1,
        marginTop:20,
        borderBottomWidth:1,
        borderLeftWidth:1,
        marginLeft:2,
        borderTopLeftRadius:10
      
    },
    titleText:{
        fontSize:15,
        fontSize:20
    },
    sdtNhan:{
        fontStyle:'italic'
    }
    
});

