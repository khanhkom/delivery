import React,{useState} from 'react'
import { View, Text,StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
export default function Title({text,icon}) {
    const [name, setname] = useState(text);

    return (
        <View style={styles.container}>
            <View style={styles.groupTitle}>
            <Icon name={icon} size={40} />
            <Text style={styles.txt}>{name}</Text>
            </View> 
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'lightskyblue',
        borderBottomWidth:1,
    },
    groupTitle:{
        flexDirection:'row',
        justifyContent:'center',
        marginTop:15
    },
    txt:{
        marginLeft:5,
        marginTop:8,
        fontSize:20,
    }
});
