/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Splash from './Splash'
let baseUrl='http://192.168.43.190:8888';

AppRegistry.registerComponent(appName, () => App);
